public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.runTheApp();
    }

    private void runTheApp() {
        String haystack = "hello";
        String needle = "ll";
        String haystack1 = "aaaaa";
        String needle1 = "bba";
        System.out.println(strStr(haystack1,needle1));
    }

    private int strStr(String haystack, String needle) {
        if (haystack.isEmpty()&&needle.isEmpty())
            return 0;
        if (haystack.isEmpty())
            return -1;
        if (needle.isEmpty())
            return 0;
        return haystack.indexOf(needle);
    }
}
